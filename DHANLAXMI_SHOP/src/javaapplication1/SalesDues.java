/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Prakriti Kumari
 */
public class SalesDues extends javax.swing.JInternalFrame {

    /**
     * Creates new form SalesDues
     */
    Statement st=null;
    Connection con=null;
     float Dues=0;
    
    public SalesDues() {
         try
        {
            System.out.println("log:::4");
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=ShopManagementSystem", "sa", "sa");
            st=con.createStatement();
        }
        catch(Exception ex) {
           ex.printStackTrace();
        }
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        totalDuesFromCustomerSide = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txt_Date_from = new com.toedter.calendar.JDateChooser();
        jLabel4 = new javax.swing.JLabel();
        txt_Date_to = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Sales Dues");
        setPreferredSize(new java.awt.Dimension(1357, 1357));

        jLabel1.setForeground(new java.awt.Color(0, 102, 153));
        jLabel1.setText("Total Dues From Customer Side");

        jButton1.setText("Show Dues");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 0, 0));
        jLabel3.setText("Sales Dues ");

        jButton3.setText("Close");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton2.setText("Save");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(0, 102, 153));
        jLabel4.setText("From");

        jLabel5.setForeground(new java.awt.Color(0, 102, 153));
        jLabel5.setText("To");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(365, 365, 365)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(251, 251, 251)
                            .addComponent(jButton1)
                            .addGap(96, 96, 96)
                            .addComponent(jButton2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton3))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(234, 234, 234)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel1)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txt_Date_from, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(37, 37, 37)
                                    .addComponent(jLabel5)
                                    .addGap(44, 44, 44)
                                    .addComponent(txt_Date_to, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(totalDuesFromCustomerSide, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(370, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel3)
                .addGap(66, 66, 66)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(txt_Date_from, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Date_to, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(63, 63, 63)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(totalDuesFromCustomerSide, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(88, 88, 88)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton2)
                    .addComponent(jButton1))
                .addContainerGap(255, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String date1= ((JTextField)txt_Date_from.getDateEditor().getUiComponent()).getText();
         String date2= ((JTextField)txt_Date_to.getDateEditor().getUiComponent()).getText();
         try {
            System.out.println("log::1");
            String sql = "select *from Customer where Date between '"+date1+"' and '"+date2+"'";
            
            
            System.out.println("log::2");
            System.out.println("Connected"+sql);
            
            ResultSet rs = st.executeQuery(sql);
             
           
            
            
            
            while (rs.next()) {
                Dues=Dues+rs.getFloat(17);     
                if(Dues>0){
                       
                    totalDuesFromCustomerSide.setText(""+Dues);}
                    else{
                            
            totalDuesFromCustomerSide.setText(""+0.0);
                }
               
              
            }
           
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         
         Dues=0;
        
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
         AddProduct2();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        int reply=JOptionPane.showConfirmDialog(null,"Are U Sure ??");
			if(reply==JOptionPane.YES_OPTION)
			{				
				dispose();
			}
    }//GEN-LAST:event_jButton3ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField totalDuesFromCustomerSide;
    private com.toedter.calendar.JDateChooser txt_Date_from;
    private com.toedter.calendar.JDateChooser txt_Date_to;
    // End of variables declaration//GEN-END:variables

    private void AddProduct2() {
        try {
              
            System.out.println("log:::1");
            Connection con;
            System.out.println("log:::2");
            Statement st;
            System.out.println("log:::3");
            ResultSet rs = null;
            System.out.println("log:::4");
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");            
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=ShopManagementSystem", "sa", "sa");
            System.out.println("log:::5");
            System.out.println("log:::6");
            st = con.createStatement();
            System.out.println("log:::7");
            System.out.println("log:::8");
            //rs=st.executeQuery("select *from railway where trainno='"+patient_code.getText()+"'");
            int i=st.executeUpdate("insert into Sale_Dues values ('"+((JTextField)txt_Date_from.getDateEditor().getUiComponent()).getText()+"','"+((JTextField)txt_Date_to.getDateEditor().getUiComponent()).getText()+"','"+Float.parseFloat(totalDuesFromCustomerSide.getText())+"')");
            System.out.println("log:::9");            
            if(i>0)
            {
              //  JOptionPane.showMessageDialog(new JFrame(),"Test Record Inserted");
                System.out.println("record inserted");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
         //  JOptionPane.showMessageDialog(new JFrame(),"Record Inserted");
        System.out.println("inserted");
      
          
          
          
    

    }
    
}
