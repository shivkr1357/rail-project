/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBConn {

    Connection con,con1;
    ResultSet rs,rs1;
    PreparedStatement stat;

    public void connection() throws ClassNotFoundException, SQLException {
        
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        System.out.println("log:::1");
        con = DriverManager.getConnection(
                "jdbc:sqlserver://localhost:1433;databaseName=ShopManagementSystem", "sa", "sa");
        System.out.println("Connecttion SuccessFul");
    }

    public void retrieve() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Purchase ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");

    }
     public void retrieve_id() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Staff_Entry ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");

    }
      public void retrieve_searchEntryID() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Staff_Entry ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");
      }
       public void retrieve_searchEntryName() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Staff_Entry ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");
      }
       public void retrieve_edit() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Staff_Entry ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");
    
       }
        public void retrieve_OfficeExpenseEntry() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM OfficeExpense_Entry ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");
    
       }
        public void retrieve_customer() throws SQLException {
        System.out.println("Db log 001");

        Statement stmt = con.createStatement();
        System.out.println("Db log 002");
        String query = "SELECT DISTINCT * FROM Customer ";
        
        System.out.println("Db log 003");
        rs = stmt.executeQuery(query);
        System.out.println("Db log 004");

        System.out.println("retrieve succesfully");
        System.out.println("Db log 005");
    
       }
}
