/*program coding of new train entry*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.applet.*;

public class entry extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno,lname,lsource,ldestination,ltype,larriv,ldepart,lcoach,lday,head;
	JTextField tno,tname,tsource,tdestination,tarriv,tdepart,tcoach;
	JComboBox ctype,cday,cday1;
	JButton save,cancel;	
	Font fnt;
	public entry()
	{
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("New Train Entry");		
		fnt=new Font("Century Gothic",Font.BOLD,20);		
		lno=new JLabel("Enter Train NO");
		lname=new JLabel("Train Name");
		lsource=new JLabel("Source Station");
		ldestination=new JLabel("Destination Station");
		ltype=new JLabel("Train Type");
		larriv=new JLabel("Arrival Time");
		ldepart=new JLabel("Departure Time");
		lday=new JLabel("Day Of Train");
		lcoach=new JLabel("Total No Of Coach");
		tno=new JTextField(20);
		tname=new JTextField(20);
		tsource=new JTextField(20);
		tdestination=new JTextField(20);		
		ctype=new JComboBox();
			ctype.addItem("Express");
			ctype.addItem("Super-Fast");
		tarriv=new JTextField(20);
		tdepart=new JTextField(20);
		cday=new JComboBox();
                        cday.addItem("daily");
			cday.addItem("Sunday");
			cday.addItem("Monday");
			cday.addItem("Tuesday");
			cday.addItem("Wednesday");
			cday.addItem("Thursday");
			cday.addItem("Friday");
			cday.addItem("Saturday");
		cday1=new JComboBox();
			cday1.addItem("Except Day");
			cday1.addItem("Except Sunday");
			cday1.addItem("Except Monday");
			cday1.addItem("Except Tuesday");
			cday1.addItem("Except Wednesday");
			cday1.addItem("Except Thursday");
			cday1.addItem("Except Friday");
			cday1.addItem("Except Saturday");
		
		tcoach=new JTextField(20);
		save=new JButton("save");
		/*save.addActionListener(this);	*/

		cancel=new JButton("Cancel");	
		cancel.addActionListener(this);		
		p.add(head);
		p.add(lno);
		p.add(tno);
	
		p.add(lname);
		p.add(tname);

		p.add(lsource);
		p.add(tsource);

		p.add(ldestination);
		p.add(tdestination);

		p.add(ltype);
		p.add(ctype);

		p.add(larriv);
		p.add(tarriv);

		p.add(ldepart);
		p.add(tdepart);
	
		p.add(lday);
		p.add(cday);
		p.add(cday1);

		p.add(lcoach);
		p.add(tcoach);
	
		p.add(save);
		save.addActionListener(this);

		p.add(cancel);

		getContentPane().add(p);
		head.setBounds(300,5,200,75);
		head.setFont(fnt);

		lno.setBounds(100,70,100,25);
		tno.setBounds(300,70,75,25);

		lname.setBounds(100,100,100,25);
		tname.setBounds(300,100,100,25);
		
		lsource.setBounds(100,130,100,25);
		tsource.setBounds(300,130,100,25);

		ldestination.setBounds(100,160,120,25);
		tdestination.setBounds(300,160,100,25);
		
		ltype.setBounds(100,190,100,25);
		ctype.setBounds(300,190,100,25);

		larriv.setBounds(100,220,100,25);
		tarriv.setBounds(300,220,100,25);

		ldepart.setBounds(100,250,100,25);
		tdepart.setBounds(300,250,100,25);
		
		lday.setBounds(100,280,100,25);
		cday.setBounds(300,280,100,25);
		cday1.setBounds(400,280,100,25);
	
		lcoach.setBounds(100,310,120,25);
		tcoach.setBounds(300,310,100,25);
		
		save.setBounds(300,400,100,25);
		cancel.setBounds(400,400,100,25);

		setSize(800,600);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==save)
		{
			try
			{
				Connection con;
				Statement st;
				ResultSet rs;
				 Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();
				rs=st.executeQuery("select *from railway where trainno='"+tno.getText()+"'");
				if(rs.next())		
				{
					JOptionPane.showMessageDialog(new JFrame(),"Record Already Exists");
				}
				else
				{
					st.executeUpdate("insert into railway values('"+tno.getText()+"','"+tname.getText()+"','"+tsource.getText()+"','"+tdestination.getText()+"','"+ctype.getSelectedItem()+"','"+tarriv.getText()+"','"+tdepart.getText()+"','"+cday.getSelectedItem()+"','"+cday1.getSelectedItem()+"','"+tcoach.getText()+"')");
					System.out.println("Connected");
					tno.setText("");
					tname.setText("");
					tsource.setText("");
					tdestination.setText("");
					ctype.setSelectedItem(this);
					tarriv.setText("");
					tdepart.setText("");
					cday.setSelectedItem(this);
					tcoach.setText("");
					JOptionPane.showMessageDialog(new JFrame(),"Record Saved ...");
				}
			}
			catch(Exception sqlexp)
			{				
				JOptionPane.showMessageDialog(new JFrame(),sqlexp);
			}
			/*catch(ClassNotFoundException cnf)
			{
				System.out.println("ERROR"+cnf);
			}*/
			
		}
		if(ae.getSource()==cancel)
		{
			dispose();
		}
	}
	public static void main(String args[])
	{
		new entry();
	}
}

			
	
	
	
		
	