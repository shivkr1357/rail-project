import javax.swing.*;
import java.applet.*;
import java.util.Date;
import java.awt.*;
import java.sql.*;
import java.awt.event.*;
public class reservation extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno,lname,lclass,lfrm,lto,lbording,lres,ldoj,ldob,ldist,lquota,ladlt,lchild,lsa,lsd,ltf,head,ldate,ldate1;
	JTextField tname,tdist,tadlt,tchild,tsa,tsd,ttf,tdate,tdoj;
	JComboBox cclass,cquota,cno,tfrm,tto,tbording,tres,tdoj1,tdob,tdob1;
	JButton go,print,cancel;		
	public reservation()
	{
		super("New Journey Form");
		p=new JPanel();
		p.setLayout(null);				
		head=new JLabel("New Journey From");
		ldate=new JLabel("Date");
		ldate1=new JLabel("");
		Date date=new Date();
		String url1=date.toString();
		String url=url1.substring(0,10);				
		ldate1.setText(url);		
		lno=new JLabel("Enter Train No");
		lname=new JLabel("Train Name");
		lclass=new JLabel("Select Class");
		lquota=new JLabel("Select Quota");
		lfrm=new JLabel("From");
		lto=new JLabel("To");
		lbording=new JLabel("Boarding From");
		lres=new JLabel("Resv upto");
		ldoj=new JLabel("Date of Journey");
		ldob=new JLabel("Date of Boarding");
		ldist=new JLabel("Distance");
		ladlt=new JLabel("No. Of Adult");
		lchild=new JLabel("No Of Child");
		lsa=new JLabel("Scheduled Arrival");
		lsd=new JLabel("Scheduled Depart");				
		ltf=new JLabel("Total Fare ");
		tdate=new JTextField(20);
		cno=new JComboBox();
			cno.addItem("Select Train No");
		tname=new JTextField(20);
		tfrm=new JComboBox();
			tfrm.addItem("From");
		tto=new JComboBox();
			tto.addItem("To");
		tbording=new JComboBox();
			tbording.addItem("patna");
		tres=new JComboBox();
			tres.addItem("delhi");
		tdoj=new JTextField(20);
			tdoj.setText("dd/mm/yy");
			tdob=new JComboBox();
			tdob.addItem("DD");
			tdob.addItem("1");
			tdob.addItem("2");
			tdob.addItem("3");
			tdob.addItem("4");
			tdob.addItem("5");
			tdob.addItem("6");
			tdob.addItem("7");
			tdob.addItem("8");
			tdob.addItem("9");
			tdob.addItem("10");			
		tdob1=new JComboBox();
			tdob1.addItem("MM");
			tdob1.addItem("1");
			tdob1.addItem("2");
			tdob1.addItem("3");
			tdob1.addItem("4");
			tdob1.addItem("5");
			tdob1.addItem("6");
			tdob1.addItem("7");
			tdob1.addItem("8");
			tdob1.addItem("9");
			tdob1.addItem("10");
			tdob1.addItem("11");
			tdob1.addItem("12");	
			tdob1.addItem("13");
			tdob1.addItem("14");
			tdob1.addItem("15");
			tdob1.addItem("17");
			tdob1.addItem("18");
			tdob1.addItem("19");
			tdob1.addItem("20");
			tdob1.addItem("21");
			tdob1.addItem("22");
			tdob1.addItem("23");
			tdob1.addItem("24");
			tdob1.addItem("25");		
			tdob1.addItem("26");
			tdob1.addItem("27");
			tdob1.addItem("28");
			tdob1.addItem("29");
			tdob1.addItem("30");
			tdob1.addItem("31");			
		tdist=new JTextField(20);
		tadlt=new JTextField(20);
		tchild=new JTextField(20);
		tsa=new JTextField(20);
		tsd=new JTextField(20);
		ttf=new JTextField(20);		
		cclass=new JComboBox();
			cclass.addItem("Sleeper");
			cclass.addItem("Ac");
			cclass.addItem("2Ac");
			cclass.addItem("3Ac");
			cclass.addItem("First Class");		
		cquota=new JComboBox();
			cquota.addItem("Tatkal");
			cquota.addItem("General");
		
		go=new JButton("Go Forward");
		print=new JButton("Print");		
		cancel=new JButton("Cancel");
		
		/*adding with panel*/
		
		p.add(head);
		p.add(lno);
		p.add(cno);

		p.add(lname);
		p.add(tname);
		
		p.add(ldate);
		p.add(ldate1);
		p.add(tdate);
		
		p.add(lclass);
		p.add(cclass);

		p.add(lfrm);
		p.add(tfrm);

		p.add(lto);
		p.add(tto);

		p.add(lbording);
		p.add(tbording);

		p.add(lres);
		p.add(tres);

		p.add(ldoj);
		p.add(tdoj);	

		p.add(ldob);
		p.add(tdob);
		p.add(tdob1);

		p.add(ldist);
		p.add(tdist);
		tdist.setEnabled(false);

		p.add(lquota);
		p.add(cquota);

		p.add(ladlt);
		p.add(tadlt);

		p.add(lchild);
		p.add(tchild);

		p.add(lsa);
		p.add(tsa);
		tsa.setEnabled(false);

		p.add(lsd);
		p.add(tsd);
		tsd.setEnabled(false);
	
		p.add(ltf);
		p.add(ttf);
		ttf.setEnabled(false);
	
		p.add(go);
		go.addActionListener(this);
		p.add(print);
		p.add(cancel);
		cancel.addActionListener(this);

		getContentPane().add(p);
		
		head.setBounds(300,5,200,75);

		lno.setBounds(50,70,100,25);
		cno.setBounds(150,70,100,25);

		lname.setBounds(50,100,100,25);
		tname.setBounds(150,100,100,25);
	
		ldate.setBounds(450,30,100,25);		
		ldate1.setBounds(500,30,100,25);		
		//tdate.setBounds(210,20,100,25);

		ldist.setBounds(490,100,100,25);
		tdist.setBounds(600,100,100,25);		
		
		lclass.setBounds(50,130,100,25);
		cclass.setBounds(150,130,100,25);	
		
		lquota.setBounds(265,130,80,25);
		cquota.setBounds(340,130,100,25);
	
		ladlt.setBounds(490,130,80,25);
		tadlt.setBounds(600,130,120,25);

		lchild.setBounds(490,160,80,25);
		tchild.setBounds(600,160,120,25);

		lsa.setBounds(490,200,120,25);		
		tsa.setBounds(600,200,120,25);		

		lsd.setBounds(490,240,120,25);		
		tsd.setBounds(600,240,120,25);		

		lfrm.setBounds(50,160,120,25);	
		tfrm.setBounds(150,160,100,25);	
		
		lto.setBounds(280,160,80,25);
		tto.setBounds(340,160,100,25);
		
		lbording.setBounds(50,200,120,25);
		tbording.setBounds(150,200,100,25);
	
		lres.setBounds(275,200,100,25);
		tres.setBounds(340,200,100,25);
		
		ldoj.setBounds(50,240,100,25);
		tdoj.setBounds(150,240,100,25);		
		
		ldob.setBounds(50,280,120,25);
		tdob.setBounds(150,280,50,25);
		tdob1.setBounds(210,280,50,25);

		ltf.setBounds(490,280,100,25);
		ttf.setBounds(600,280,100,25);				
		
		go.setBounds(200,400,100,25);	
		print.setBounds(300,400,100,25);
		cancel.setBounds(400,400,100,25);
	
		setSize(800,600);
		setVisible(true);
		fill();
		fillfrom();
		fillto();
		fillresupto();
		fillboarding();
	}	
	public static void main(String args[])
	{		
		new reservation();		
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==go)
		{
			try
			{
				Connection con;
				Statement st,st1;
				ResultSet rs,rs1;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");	 
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();	
				st1=con.createStatement();		
				String sql1="select *from railway where trainno='"+cno.getSelectedItem()+"'";
				rs1=st.executeQuery(sql1);
				if(rs1.next())
				{
					tname.setText(rs1.getString(2));
				}
				
				String sql="select *from stoppage where tno='"+cno.getSelectedItem()+"'";
				rs=st1.executeQuery(sql);
				String str=tadlt.getText();
				if(str.length()==0)
				{
					JOptionPane.showMessageDialog(new JFrame(),"Please Enter No of person");
					tadlt.requestFocus();
					return;
				}
				String str2="0";
				String str3="0";
				String str4="0";
				if(rs.next())
				{
					str2=rs.getString(3);
					str2.trim();
					str3=rs.getString(4);
					str3.trim();
					str4=rs.getString(5);
					str4.trim();
					
				}
				tdist.setText(str2);
				tsa.setText(str3);
				tsd.setText(str4);
				String str1=tadlt.getText();
				String str5=tchild.getText();
				int ch=Integer.parseInt(str5);				
				int n=Integer.parseInt(str1);				
				int m=Integer.parseInt(str2);
				double f=n*(m/10);
				double f1=n*(m/5);
				double f3=f+f1;
				String ff=""+f3;
				ttf.setText(ff);
				
				
					
								
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
		}
		if(ae.getSource()==cancel)
		{
			dispose();
		}	
	}
	public void fill()
	{
		try
		{
			Connection con;
			Statement st;
			ResultSet rs;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			String sq="Select * from railway";
			rs=st.executeQuery(sq);
			while(rs.next())
			{
				cno.addItem(rs.getString(1));				
			}
		}
		catch(Exception ex)
		{
		}
	}
	public void fillfrom()
	{
		try
		{
			Connection con;
			ResultSet rs;
			Statement st;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			String sql2="select *from railway" ;
			rs=st.executeQuery(sql2);
			while(rs.next())
			{
				tfrm.addItem(rs.getString(3));
			}
		}
		catch(Exception ex)
		{
		}
	}
	public void fillto()
	{
		try
		{
			Connection con;
			ResultSet rs;
			Statement st;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			String sql3="select *from railway";
			rs=st.executeQuery(sql3);
			while(rs.next())
			{
				tto.addItem(rs.getString(4));
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error"+ex);
		}
	}
	public void fillboarding()
	{
		try
		{
			Connection con;
			ResultSet rs;
			Statement st;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			String sql2="select trsource from railway" ;
			rs=st.executeQuery(sql2);
			while(rs.next())
			{
				tfrm.addItem(rs.getString(4));
			}
		}
		catch(Exception ex)
		{
		}		
	}
	public void fillresupto()
	{
		try
		{
			Connection con;
			ResultSet rs;
			Statement st;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			String sql3="select *from railway where tno='"+cno.getSelectedItem()+"'";
			rs=st.executeQuery(sql3);
			while(rs.next())
			{
				tres.addItem(rs.getString(4));
			}
		}
		catch(Exception ex)
		{
			System.out.println("Error"+ex);
		}
	}
				
		
}


	
	