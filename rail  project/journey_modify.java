import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.awt.event.*;
public class journey_modify extends JFrame /*implements ActionListener*/
{
	JPanel p;
	JLabel lno,lname,lclass,lfrm,lto,lbording,lres,ldoj,ldob,ldist,lquota,ladlt,lchild,lsa,lsd,ltf,head;
	JTextField tname,tfrm,tto,tbording,tres,tdoj,tdob,tdist,tadlt,tchild,tsa,tsd,ttf;
	JComboBox cclass,cquota,cno;
	JButton go,print,cancel;
	public journey_modify()
	{
		super("Journey Modify Form");
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("New Journey From");
		lno=new JLabel("Enter Train No");
		lname=new JLabel("Train Name");
		lclass=new JLabel("Select Class");
		lquota=new JLabel("Select Quota");
		lfrm=new JLabel("From");
		lto=new JLabel("To");
		lbording=new JLabel("Boarding From");
		lres=new JLabel("Resv upto");
		ldoj=new JLabel("Date of Journey");
		ldob=new JLabel("Date of Boarding");
		ldist=new JLabel("Distance");
		ladlt=new JLabel("No. Of Adult");
		lchild=new JLabel("No Of Child");
		lsa=new JLabel("Scheduled Arrival");
		lsd=new JLabel("Scheduled Depart");				
		ltf=new JLabel("Total Fare ");
		cno=new JComboBox();
			cno.addItem("Select Train No");
		tname=new JTextField(20);
		tfrm=new JTextField(20);
		tto=new JTextField(20);
		tbording=new JTextField(20);
		tres=new JTextField(20);
		tdoj=new JTextField(20);
		tdob=new JTextField(20);
		tdist=new JTextField(20);
		tadlt=new JTextField(20);
		tchild=new JTextField(20);
		tsa=new JTextField(20);
		tsd=new JTextField(20);
		ttf=new JTextField(20);		
		cclass=new JComboBox();
			cclass.addItem("Sleeper");
			cclass.addItem("Ac");
			cclass.addItem("2Ac");
			cclass.addItem("3Ac");
			cclass.addItem("First Class");		
		cquota=new JComboBox();
			cquota.addItem("Tatkal");
			cquota.addItem("General");
		
		go=new JButton("Go Forward");
		print=new JButton("Print");		
		cancel=new JButton("Cancel");
		
		/*adding with panel*/
		
		p.add(head);
		p.add(lno);
		p.add(cno);

		p.add(lname);
		p.add(tname);

		p.add(lclass);
		p.add(cclass);

		p.add(lfrm);
		p.add(tfrm);

		p.add(lto);
		p.add(tto);

		p.add(lbording);
		p.add(tbording);

		p.add(lres);
		p.add(tres);

		p.add(ldoj);
		p.add(tdoj);	

		p.add(ldob);
		p.add(tdob);

		p.add(ldist);
		p.add(tdist);
		tdist.setEnabled(false);

		p.add(lquota);
		p.add(cquota);

		p.add(ladlt);
		p.add(tadlt);

		p.add(lchild);
		p.add(tchild);

		p.add(lsa);
		p.add(tsa);
		tsa.setEnabled(false);

		p.add(lsd);
		p.add(tsd);
		tsd.setEnabled(false);
	
		p.add(ltf);
		p.add(ttf);
		ttf.setEnabled(false);
	
		p.add(go);
		p.add(print);
		p.add(cancel);

		getContentPane().add(p);
		
		head.setBounds(300,5,200,75);

		lno.setBounds(50,70,100,25);
		cno.setBounds(150,70,75,25);

		lname.setBounds(50,100,100,25);
		tname.setBounds(150,100,100,25);
		
		ldist.setBounds(490,100,120,25);
		tdist.setBounds(600,100,120,25);		
		
		lclass.setBounds(50,130,100,25);
		cclass.setBounds(150,130,100,25);	
		
		lquota.setBounds(265,130,80,25);
		cquota.setBounds(340,130,100,25);
	
		ladlt.setBounds(490,130,80,25);
		tadlt.setBounds(600,130,120,25);

		lchild.setBounds(490,160,80,25);
		tchild.setBounds(600,160,120,25);

		lsa.setBounds(490,200,120,25);		
		tsa.setBounds(600,200,120,25);		

		lsd.setBounds(490,240,120,25);		
		tsd.setBounds(600,240,120,25);		

		lfrm.setBounds(50,160,120,25);	
		tfrm.setBounds(140,160,120,25);	
		
		lto.setBounds(280,160,80,25);
		tto.setBounds(320,160,120,25);
		
		lbording.setBounds(50,200,120,25);
		tbording.setBounds(140,200,120,25);
	
		lres.setBounds(275,200,100,25);
		tres.setBounds(330,200,110,25);
		
		ldoj.setBounds(50,240,100,25);
		tdoj.setBounds(140,240,100,25);
		
		ldob.setBounds(50,280,120,25);
		tdob.setBounds(140,280,120,25);

		ltf.setBounds(490,280,100,25);
		ttf.setBounds(600,280,100,25);				
		
		go.setBounds(200,400,100,25);	
		print.setBounds(300,400,100,25);
		cancel.setBounds(400,400,100,25);
	
		setSize(800,600);
		setVisible(true);
	}	
	public static void main(String args[])
	{
		new journey_modify();
	}
}


	
	