/*program coding of new train entry*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
public class view extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno,lname,lsource,ldestination,ltype,larriv,ldepart,lcoach,lday,head;
	JTextField tno,tname,tsource,tdestination,tarriv,tdepart,tcoach;
	JComboBox ctype,cday;
	JButton view,cancel;
	Font fnt;
	public view()
	{
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("View Train Entry");
		
		fnt=new Font("Century Gothic",Font.BOLD,20);		
		lno=new JLabel("Enter Train NO");
		lname=new JLabel("Train Name");
		lsource=new JLabel("Source Station");
		ldestination=new JLabel("Destination Station");
		ltype=new JLabel("Train Type");
		larriv=new JLabel("Arrival Time");
		ldepart=new JLabel("Departure Time");
		lday=new JLabel("Day Of Train");
		lcoach=new JLabel("Total No Of Coach");
		tno=new JTextField(20);
		tname=new JTextField(20);
		tsource=new JTextField(20);
		tdestination=new JTextField(20);		
		ctype=new JComboBox();
			ctype.addItem("Express");
			ctype.addItem("Super-Fast");
		tarriv=new JTextField(20);
		tdepart=new JTextField(20);
		cday=new JComboBox();
			cday.addItem("Sunday");
			cday.addItem("Monday");
			cday.addItem("Tuesday");
			cday.addItem("Wednesday");
			cday.addItem("Thursday");
			cday.addItem("Friday");
			cday.addItem("Saturday");
		
		tcoach=new JTextField(20);
		view=new JButton("view");	
		cancel=new JButton("Cancel");	
			
		p.add(head);
		p.add(lno);
		p.add(tno);
	
		p.add(lname);
		p.add(tname);

		p.add(lsource);
		p.add(tsource);

		p.add(ldestination);
		p.add(tdestination);

		p.add(ltype);
		p.add(ctype);

		p.add(larriv);
		p.add(tarriv);

		p.add(ldepart);
		p.add(tdepart);
	
		p.add(lday);
		p.add(cday);

		p.add(lcoach);
		p.add(tcoach);
	
		p.add(view);
		view.addActionListener(this);

		p.add(cancel);

		getContentPane().add(p);				
		head.setBounds(300,5,200,75);
		head.setFont(fnt);

		lno.setBounds(100,70,100,25);
		tno.setBounds(300,70,75,25);

		lname.setBounds(100,100,100,25);
		tname.setBounds(300,100,100,25);
		
		lsource.setBounds(100,130,100,25);
		tsource.setBounds(300,130,100,25);

		ldestination.setBounds(100,160,120,25);
		tdestination.setBounds(300,160,100,25);
		
		ltype.setBounds(100,190,100,25);
		ctype.setBounds(300,190,100,25);

		larriv.setBounds(100,220,100,25);
		tarriv.setBounds(300,220,100,25);

		ldepart.setBounds(100,250,100,25);
		tdepart.setBounds(300,250,100,25);
		
		lday.setBounds(100,280,100,25);
		cday.setBounds(300,280,100,25);

		lcoach.setBounds(100,310,120,25);
		tcoach.setBounds(300,310,100,25);
		
		view.setBounds(300,400,100,25);
		cancel.setBounds(400,400,100,25);

		setSize(800,600);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==view)
		{
			try
			{
				Connection con;
				Statement st;
				ResultSet rs;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();				
				String sql="select *from railway where trainno='"+tno.getText()+"'";
				System.out.println("Connected");			
				rs=st.executeQuery(sql);				
				if(rs.next())
				{				
					tno.setText(rs.getString(1));
					tname.setText(rs.getString(2));
					tsource.setText(rs.getString(3));
					tdestination.setText(rs.getString(4));
					ctype.setSelectedItem(rs.getString(5));
					tarriv.setText(rs.getString(6));
					tdepart.setText(rs.getString(7));
					cday.setSelectedItem(rs.getString(8));
					tcoach.setText(rs.getString(9));
					JOptionPane.showMessageDialog(new JFrame(),"next");
				}
				else
				{
					JOptionPane.showMessageDialog(new JFrame(),"Not Found");
				}
			}
			catch(SQLException sqlexp)
			{
				System.out.println("ERROR"+sqlexp);
			}
			catch(ClassNotFoundException cnf)
			{
				System.out.println("ERROR"+cnf);
			}
		}
	}
	public static void main(String args[])
	{
		new view();
	}
}

			
	
	
	
		
	