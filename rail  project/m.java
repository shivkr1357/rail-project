/*Coding of train menu*/
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class sp_train extends JFrame implements ActionListener
{
	JMenuBar jmb;
	JMenu train,stoppage,journey,ticket,help;
	JMenuItem t1,t2,t3,t4,s1,s2,s3,s4,j1,j2,j3,j4,ti1,ti2,ti3,h1,h2,h3;	
	JMenuItem ts1,ts2,ts3,ts4,ts5;
	JMenuItem ssno,ssname;
	JMenuItem js1,js2;
	JPanel p;
	//JFrame fe;
	
	public sp_train()
	{
		
		p=new JPanel();
		jmb=new JMenuBar();
		train=new JMenu("Train",true);
		stoppage=new JMenu("Stoppage",true);
		journey=new JMenu("Journey",true);
		ticket=new JMenu("Ticket",true);
		help=new JMenu("Help",true);
		t1=new JMenuItem("New");
		t2=new JMenuItem("Modify");				
		t3=new JMenu("Search");		
			ts1=new JMenuItem("Train No.");
			ts2=new JMenuItem("Name");			
			ts3=new JMenuItem("Source");
			ts4=new JMenuItem("Destination");
			ts5=new JMenuItem("Type");
			t3.add(ts1);
			t3.add(ts2);
			t3.add(ts3);
			t3.add(ts4);
			t3.add(ts5);
		t4=new JMenuItem("Exit");		
		s1=new JMenuItem("New");
		s2=new JMenuItem("Modify");		
		s3=new JMenuItem("Delete");
		s4=new JMenu("Search");
			ssno=new JMenuItem("Train No.");
			ssname=new JMenuItem("Train Name");
			s4.add(ssno);
			//s4.add(ssname);
		j1=new JMenuItem("New");
		j2=new JMenuItem("Modify");
		j3=new JMenuItem("Delete");
		//j4=new JMenu("Search");
			/*js1=new JMenuItem("Train No.");
			js2=new JMenuItem("Train Name");
			j4.add(js1);
			j4.add(js2);*/
		ti1=new JMenuItem("Reservation");
		ti2=new JMenuItem("Cancellation");
		ti3=new JMenuItem("Status");		
		h1=new JMenuItem("About me");
		h2=new JMenuItem("software");
		h3=new JMenuItem("Language");
		train.add(t1);
		t1.addActionListener(this);
		train.setMnemonic('t');

		train.add(t2);
		t2.addActionListener(this);
		train.addSeparator();		

		train.add(t3);
		ts1.addActionListener(this);
		ts2.addActionListener(this);
		ts3.addActionListener(this);
		ts4.addActionListener(this);
		ts5.addActionListener(this);

		jmb.add(train);
		train.add(t4);
		t4.addActionListener(this);
		stoppage.setMnemonic('s');		
		stoppage.add(s1);
		s1.addActionListener(this);
		stoppage.add(s2);
		s2.addActionListener(this);
		stoppage.add(s3);
		s3.addActionListener(this);
		stoppage.add(s4);
		s4.add(ssno);
		ssno.addActionListener(this);
		journey.setMnemonic('j');		
		j1.addActionListener(this);
		j2.addActionListener(this);
		j3.addActionListener(this);
		journey.add(j1);
		journey.add(j2);
		journey.add(j3);
		//journey.add(j4);
		ticket.setMnemonic('i');		
		//ti1.addActionListener(this);
		ticket.add(ti1);
		ticket.add(ti2);
		ticket.add(ti3);
		help.setMnemonic('h');		
		help.add(h1);
		help.add(h2);
		help.add(h3);				
		jmb.add(stoppage);	
		jmb.add(journey);	
		jmb.add(ticket);	
		jmb.add(help);			
		p.add(jmb);		
		getContentPane().add(p);
		p.setBackground(Color.gray);
		setSize(800,600);				
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==t1)
		{		
			entry en=new entry();
		}
		if(ae.getSource()==t2)
		{
			modify mo=new modify();
		}
		if(ae.getSource()==ts1)
		{
			search_no se=new search_no();
		}
		if(ae.getSource()==ts2)
		{
			search_name sn=new search_name();
		}
		if(ae.getSource()==ts3)
		{
			search_source ss=new search_source();
		}
		if(ae.getSource()==ts4)		
		{
			search_destination sd=new search_destination();
		}
		if(ae.getSource()==ts5)
		{
			search_type st=new search_type();
		}
		if(ae.getSource()==s1)
		{
			stoppage_entry stop =new stoppage_entry();
		}
		if(ae.getSource()==s2)
		{
			stoppage_modify stopmod =new stoppage_modify();
		}
		if(ae.getSource()==s3)
		{
			stoppage_delete stdel=new stoppage_delete();
		}
		if(ae.getSource()==ssno)
		{
			stoppage_search_no sno=new stoppage_search_no();
		}
		if(ae.getSource()==j1)
		{
			journey_entry jent=new journey_entry();
		}
		if(ae.getSource()==j2)
		{
			journey_modify mody=new journey_modify();
		}
		if(ae.getSource()==j3)
		{
			journey_delete jdel=new journey_delete();
		}
		if(ae.getSource()==t4)		
		{
			dispose();
		}
		
	}
	
	public static void main(String args[])
	{	
		new sp_train();
	}
}
	
