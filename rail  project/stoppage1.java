/*program coding of stoppage train entry*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
public class stoppage1 extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno,lsource,lstop,ldistance,larriv,ldepart,head,ldist;
	JTextField tno,tsource,tstop,tdistance,tarriv,tdepart,tdist;
	JTable table;	
	String data;
	JButton save,cancel,check,ex;	
	public stoppage1()
	{
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("Stoppage  Entry Of Train");		
		lno=new JLabel("Enter Train NO");
		lsource=new JLabel("Source of Train");
		ldist=new JLabel("Distination of Train");
		lstop=new JLabel("Stoppage Of Train");		
		ldistance=new JLabel("Distance from source");
		larriv=new JLabel("Arrival Time");
		ldepart=new JLabel("Departure Time");
		tno=new JTextField(20);
		tsource=new JTextField(20);
		tdist=new JTextField(20);
		tstop=new JTextField(20);		
		tdistance=new JTextField(20);		
		tarriv=new JTextField(20);
		tdepart=new JTextField(20);
		save=new JButton("save");
		save.addActionListener(this);
		cancel=new JButton("Cancel");
		cancel.addActionListener(this);		
		check=new JButton("Check Train no");
		check.addActionListener(this);
		ex=new JButton("exit");
		ex.addActionListener(this);

		p.add(head);
		p.add(lno);
		p.add(tno);
		
		p.add(lsource);
		p.add(tsource);
	
		p.add(ldist);
		p.add(tdist);

		p.add(lstop);
		p.add(tstop);
		
		p.add(ldistance);
		p.add(tdistance);

		p.add(larriv);
		p.add(tarriv);

		p.add(ldepart);
		p.add(tdepart);	
		p.add(check);		
		p.add(save);	
		p.add(ex);

		p.add(cancel);
		cancel.addActionListener(this);
		getContentPane().add(p);				
		head.setBounds(300,5,200,75);		

		lno.setBounds(200,70,100,25);
		tno.setBounds(400,70,75,25);
				
		check.setBounds(500,70,150,25);

		lsource.setBounds(200,120,150,25);
		tsource.setBounds(400,120,100,25);
		
		ldist.setBounds(200,170,150,25);
		tdist.setBounds(400,170,100,25);

		lstop.setBounds(200,220,150,25);
		tstop.setBounds(400,220,100,25);		
		
		ldistance.setBounds(200,270,150,25);
		tdistance.setBounds(400,270,100,25);	
		
		larriv.setBounds(200,320,100,25);
		tarriv.setBounds(400,320,100,25);

		ldepart.setBounds(200,370,100,25);
		tdepart.setBounds(400,370,100,25);		

		save.setBounds(200,400,100,25);
		cancel.setBounds(400,400,100,25);
		ex.setBounds(600,400,100,25);

		setSize(800,600);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==save)
		{
			try
			{
				
				Connection con;
				ResultSet rs;
				Statement st;
				
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:stoppage");
				st=con.createStatement();
				rs=st.executeQuery("select *from stoppage ");
				System.out.println("Connected Successfully");
					
					data=tno.getText();
					if((data.trim()).length()==0)
					{
						JOptionPane.showMessageDialog(new JFrame(),"train no cannot be blank");
						tno.requestFocus();
						return;
					}
					
								data=tsource.getText();
					/*if((data.trim().length()==0))
					{
						JOptionPane.showMessageDialog(new JFrame(),"Source Field cannot be blank!!");
					}*/
					data=tstop.getText();
					if((data.trim()).length()==0)
					{
						JOptionPane.showMessageDialog(new JFrame(),"stoppage field cannot be blank");
						tno.requestFocus();
						return;//else
					}
					//{
						st.executeQuery("insert into stoppage values('"+tno.getText()+"'','"+tstop.getText()+"','"+tdistance.getText()+"','"+tarriv.getText()+"','"+tdepart.getText()+"')");
															
					System.out.println("Connected");
					tno.setText("");
					tsource.setText("");
					tstop.setText("");
					tdistance.setText("");					
					tarriv.setText("");
					tdepart.setText("");					
					
					JOptionPane.showMessageDialog(new JFrame(),"Record Saved ...");
					int reply=JOptionPane.showConfirmDialog(null,"Enter next stoppage???");
					if(reply==JOptionPane.YES_OPTION)
					{		
						tno.setEnabled(false);				
						//tsource.setVisible(false);
						//lsource.setVisible(false);						
						st.executeQuery("insert into stoppage values('"+tno.getText()+"','"+tstop.getText()+"','"+tdistance.getText()+"','"+tarriv.getText()+"','"+tdepart.getText()+"')");
						JOptionPane.showMessageDialog(null,"Record saved...","note",2);					
					}				
					else
					{
						JOptionPane.showMessageDialog(null,"Nothing to Update..","note",2);
						return;
					}
			}
			catch(Exception sqlexp)
			{
				System.out.println("ERROR"+sqlexp);				
			}
				//tno.setEnabled(true);
		}
		if(ae.getSource()==check)
		{
		try
		{
			Statement st;
			Connection con;
			ResultSet rs;
			String data;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();
			rs=st.executeQuery("select *from railway ");
			System.out.println("Connected Successfully");		
			data="select *from railway where trainno='"+tno.getText()+"'";
			rs=st.executeQuery(data);
			if((data.trim()).length()==0)
			{
					JOptionPane.showMessageDialog(new JFrame(),"train no cannot be blank");
					tno.requestFocus();
					return;
			}
			if(rs.next())
			{
					
				JOptionPane.showMessageDialog(new JFrame(),"Train is available now procced");
				tsource.setText(rs.getString(3));
				tsource.setEnabled(false);
				tdist.setText(rs.getString(4));
				tdist.setEnabled(false);
				tno.setEnabled(false);				
			}
				
			else
			{
				JOptionPane.showMessageDialog(null,"Train is not available sorry!!!","note",2);
				tno.requestFocus();
				tno.setEnabled(true);
			}
		}
		catch(Exception sqlexp)
		{
			System.out.println("ERROR"+sqlexp);				
		}
	}
		
		
		if(ae.getSource()==cancel)
		{
			tno.setEnabled(true);			
			tsource.setText("");
			tdist.setText("");
			tstop.setText("");
			tdistance.setText("");					
			tarriv.setText("");
			tdepart.setText("");					
		}
		if(ae.getSource()==ex)
		{
			dispose();
		}			
}
	public static void main(String args[])
	{
		new stoppage1();
	}
}

			
	
	
	
		
	