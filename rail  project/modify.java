 /*program coding of new train entry*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import javax.swing.event.*;
public class modify extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno, lname, lsource, ldestination, ltype, larriv, ldepart, lcoach, lday, head;
	JTextField tno, tname, tsource, tdestination, tarriv, tdepart, tcoach;
	JComboBox ctype, cday, cday1;
	JButton search, modify, save, cancel, searcha;
	Font fnt;
	public modify()
	{
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("Search & Modify Train Entry");
		
		fnt=new Font("Century Gothic",Font.BOLD,20);		
		lno=new JLabel("Enter Train NO");
		lname=new JLabel("Train Name");
		lsource=new JLabel("Source Station");
		ldestination=new JLabel("Destination Station");
		ltype=new JLabel("Train Type");
		larriv=new JLabel("Arrival Time");
		ldepart=new JLabel("Departure Time");
		lday=new JLabel("Day Of Train");
		lcoach=new JLabel("Total No Of Coach");
		tno=new JTextField(20);
		tname=new JTextField(20);
		tsource=new JTextField(20);
		tdestination=new JTextField(20);		
		ctype=new JComboBox();
			ctype.addItem("Train Type");
			ctype.addItem("Express");
			ctype.addItem("Super-Fast");
		tarriv=new JTextField(20);
		tdepart=new JTextField(20);
		cday=new JComboBox();
			cday.addItem("Select Day");
			cday.addItem("Daily");
			cday.addItem("Sunday");
			cday.addItem("Monday");
			cday.addItem("Tuesday");
			cday.addItem("Wednesday");
			cday.addItem("Thursday");
			cday.addItem("Friday");
			cday.addItem("Saturday");
		cday1=new JComboBox();
			cday1.addItem("Except Day");
			cday1.addItem("Except Sunday");
			cday1.addItem("Except Monday");
			cday1.addItem("Except Tuesday");
			cday1.addItem("Except Wednesday");
			cday1.addItem("Except Thursday");
			cday1.addItem("Except Friday");
			cday1.addItem("Except Saturday");
		
		tcoach=new JTextField(20);
	
		searcha=new JButton("search again");
		search=new JButton("search");	
		modify=new JButton("modify");
		save=new JButton("save");
		cancel=new JButton("Cancel");	
			
		p.add(head);
		p.add(lno);
		p.add(tno);
	
		p.add(lname);
		p.add(tname);

		p.add(lsource);
		p.add(tsource);

		p.add(ldestination);
		p.add(tdestination);

		p.add(ltype);
		p.add(ctype);

		p.add(larriv);
		p.add(tarriv);

		p.add(ldepart);
		p.add(tdepart);
	
		p.add(lday);
		p.add(cday);
		p.add(cday1);
	
		p.add(lcoach);
		p.add(tcoach);

	/*Button*/	
		p.add(search);
		search.addActionListener(this);
		
		p.add(searcha);
		searcha.addActionListener(this);

		p.add(modify);
		modify.addActionListener(this);		

		p.add(save);		
		save.addActionListener(this);

		p.add(cancel);
		cancel.addActionListener(this);

		getContentPane().add(p);				
		head.setBounds(300,5,300,75);
		head.setFont(fnt);

		lno.setBounds(100,70,100,25);
		tno.setBounds(300,70,75,25);

		lname.setBounds(100,100,100,25);
		tname.setBounds(300,100,100,25);
		tname.setEnabled(false);

		lsource.setBounds(100,130,100,25);
		tsource.setBounds(300,130,100,25);
		tsource.setEnabled(false);

		ldestination.setBounds(100,160,120,25);
		tdestination.setBounds(300,160,100,25);
		tdestination.setEnabled(false);
		
		ltype.setBounds(100,190,100,25);
		ctype.setBounds(300,190,100,25);
		ctype.setEnabled(false);

		larriv.setBounds(100,220,100,25);
		tarriv.setBounds(300,220,100,25);
		tarriv.setEnabled(false);

		ldepart.setBounds(100,250,100,25);
		tdepart.setBounds(300,250,100,25);
		tdepart.setEnabled(false);
		
		lday.setBounds(100,280,100,25);
		cday.setBounds(300,280,100,25);
		cday1.setBounds(400,280,150,25);
		cday.setEnabled(false);
		cday1.setEnabled(false);

		lcoach.setBounds(100,310,120,25);
		tcoach.setBounds(300,310,100,25);
		tcoach.setEnabled(false);
		
		searcha.setBounds(200,400,120,25);
		search.setBounds(100,400,100,25);
		modify.setBounds(300,400,100,25);
		save.setBounds(400,400,100,25);
		cancel.setBounds(500,400,100,25);

		setSize(800,600);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==search)
		{
			try
			{
				Connection con;
				Statement st;
				ResultSet rs;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();				
				String str=tno.getText();
				if(str.length()==0)				
				{
					JOptionPane.showMessageDialog(null,"Search field Cannot be Blank!!!","note",2);
					tno.requestFocus();
					return;
				}			
                                                                         	String sql="select *from railway where trainno='"+tno.getText()+"'";
				//Integer.parseInt(tno).getInt()";
                                                                          	rs=st.executeQuery(sql);
				if(rs.next())
				{				
					tname.setText(rs.getString(2));
					tsource.setText(rs.getString(3));
					tdestination.setText(rs.getString(4));
					ctype.setSelectedItem(rs.getString(5));
					tarriv.setText(rs.getString(6));
					tdepart.setText(rs.getString(7));
					cday.setSelectedItem(rs.getString(8));
					cday1.setSelectedItem(rs.getString(9));
					tcoach.setText(rs.getString(10));
					modify.setEnabled(true);
				}			
				else
				{
					JOptionPane.showMessageDialog(new JFrame(),"Not Found");
					tno.setText("");
					tname.setText("");
					tsource.setText("");
					tdestination.setText("");
					ctype.setSelectedItem(this);
					tarriv.setText("");
					tdepart.setText("");
					cday.setSelectedItem(this);
					cday1.setSelectedItem(this);
					tcoach.setText("");
					modify.setEnabled(false);
				}
			}
			catch(Exception ex)
			{
				System.out.println("ERROR"+ex);
			}			
		}	
		if(ae.getSource()==modify)
		{		
			tno.setEnabled(false);
			tname.setEnabled(true);
			tsource.setEnabled(true);
			tdestination.setEnabled(true);
			tarriv.setEnabled(true);
			tdepart.setEnabled(true);
			tcoach.setEnabled(true);
			ctype.setEnabled(true);
			cday.setEnabled(true);
			cday1.setEnabled(true);
		}
		if(ae.getSource()==save)
		{
			try
			{			
				Connection con;
				Statement st;				
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();
				String tvalue=tno.getText();
				if(tvalue.length()==0)
				{
					JOptionPane.showMessageDialog(null,"Train number cannot be blank!!!","note",2);
					tno.requestFocus();
					return;
				}
				int reply=JOptionPane.showConfirmDialog(null,"Save Changes ??");
				if(reply==JOptionPane.YES_OPTION)
				{
					String sql1="UPDATE railway SET  trainname='"+tname.getText()+"',trsource='"+tsource.getText()+"',trdest='"+tdestination.getText()+"',trtype='"+ctype.getSelectedItem()+"',trarriv='"+tarriv.getText()+"',trdepart='"+tdepart.getText()+"',trday='"+cday.getSelectedItem()+"',trexday='"+cday1.getSelectedItem()+"',trcoach='"+tcoach.getText()+"'  WHERE trainno='"+tno.getText()+"'";
					int res=st.executeUpdate(sql1);
					JOptionPane.showMessageDialog(new JFrame(),"Record Updated");			
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Nothing to Update");					
					return;
				}								
			}
                   		     catch(Exception ex)
			{
				System.out.println("Error "+ex);
                        		}
		}
		if(ae.getSource()==cancel)
		{
			int reply=JOptionPane.showConfirmDialog(null,"Are U Sure ??");
			if(reply==JOptionPane.YES_OPTION)
			{
				dispose();
			}
		}
		if(ae.getSource()==searcha)
		{
			tno.setEnabled(true);			
			tname.setEnabled(false);
			tsource.setEnabled(false);
			tdestination.setEnabled(false);
			tarriv.setEnabled(false);
			tdepart.setEnabled(false);
			tcoach.setEnabled(false);
			ctype.setEnabled(false);
			cday.setEnabled(false);
			cday1.setEnabled(false);
		}
			
	}
	public static void main(String args[])
	{
		modify ob=new modify();
	}
  
  
  }
Shri Siddhi Vinayak
Ganga singh market 1st flour 
Gola Bazar Sonpur (Saran)
9031410695
9852933223

  
  C:\Users\satyajeet\Documents\NetBeansProjects\JavaApplication1\src\javaapplication1\Logo_B&W.png
  
  <band height="79" splitType="Stretch"/>
  
  
  
  
  
  
  
  
  
  * No guarantee for jari and color 
* No cash refund 
* Exchange within 7 days with bill and tags
* Exchange time 12:00 pm to 4:00 pm except Sunday
* customer help line no ##########
* Thanks please visit again



Co

9308621420
7870555194



