/*program coding of modify stoppage entry*/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.io.*;
public class stoppage_search_no extends JFrame implements ActionListener
{
	JPanel p;
	JLabel lno,lname,lsource,ldestination,ltype,larriv,ldepart,lcoach,lday,head;
	JLabel slstop,slarriv,sldepart,sldist,slmod;
	JTextField tname,tsource,tdestination,tarriv,tdepart,tcoach;
	JTextField starriv,stdepart,stdist,tstop;
	JComboBox ctype,cday,cday1,cno,ststop,tno;
	JButton search,stop,detail,cancel,searcha,savestop;
	Font fnt;
	public stoppage_search_no()
	{
		super("Stoppage Display By Number Screen");
		p=new JPanel();
		p.setLayout(null);
		head=new JLabel("STOPPAGE DISPLAY BY TRAIN NO");
		
		fnt=new Font("Century Gothic",Font.BOLD,20);		
		lno=new JLabel("Enter Train NO");
		lname=new JLabel("Train Name");
		slstop=new JLabel("Stoppage Of Train");		
		sldist=new JLabel("Distance from source");
		lsource=new JLabel("Source Station");
		ldestination=new JLabel("Destination Station");
		ltype=new JLabel("Train Type");
		larriv=new JLabel("Arrival Time");
		slarriv=new JLabel("Train Arrival Time ");
		ldepart=new JLabel("Departure Time");
		sldepart=new JLabel("Train Departure Time");
		lday=new JLabel("Day Of Train");
		lcoach=new JLabel("Total No Of Coach");
		tno=new JComboBox();
		cno=new JComboBox();
		cno.addItem("Select Train No");
		//stmod=new JTextField(20);
		tname=new JTextField(20);
		ststop=new JComboBox();
		ststop.addItem("Select Stoppage");
		tsource=new JTextField(20);
		tdestination=new JTextField(20);		
		stdist=new JTextField(20);
		ctype=new JComboBox();
			ctype.addItem("Train Type");
			ctype.addItem("Express");
			ctype.addItem("Super-Fast");
		tarriv=new JTextField(20);
		starriv=new JTextField(20);
		tdepart=new JTextField(20);
		stdepart=new JTextField(20);
		cday=new JComboBox();
			cday.addItem("Select Day");
			cday.addItem("Daily");
			cday.addItem("Sunday");
			cday.addItem("Monday");
			cday.addItem("Tuesday");
			cday.addItem("Wednesday");
			cday.addItem("Thursday");
			cday.addItem("Friday");
			cday.addItem("Saturday");
		cday1=new JComboBox();
			cday1.addItem("Except Day");
			cday1.addItem("Except Sunday");
			cday1.addItem("Except Monday");
			cday1.addItem("Except Tuesday");
			cday1.addItem("Except Wednesday");
			cday1.addItem("Except Thursday");
			cday1.addItem("Except Friday");
			cday1.addItem("Except Saturday");
		
		tcoach=new JTextField(20);
	
		searcha=new JButton("search again");
		search=new JButton("search");	
		stop=new JButton("stoppage");
		detail=new JButton("detail");
		cancel=new JButton("Cancel");	
			
		p.add(head);
		p.add(lno);
		p.add(cno);
	
		p.add(lname);
		p.add(tname);

		p.add(slstop);
		p.add(ststop);

		//p.add(slmod);
		//p.add(stmod);

		p.add(lsource);
		p.add(tsource);

		p.add(ldestination);
		p.add(tdestination);
		
		p.add(sldist);
		p.add(stdist);

		p.add(ltype);
		p.add(ctype);

		p.add(larriv);
		p.add(tarriv);
		
		p.add(slarriv);
		p.add(starriv);
		
		p.add(ldepart);
		p.add(tdepart);
		
		p.add(sldepart);
		p.add(stdepart);
	
		p.add(lday);
		p.add(cday);
		p.add(cday1);
	
		p.add(lcoach);
		p.add(tcoach);

	/*Button*/	
		p.add(search);
		search.addActionListener(this);
		
		p.add(searcha);
		searcha.addActionListener(this);

		p.add(stop);
		stop.addActionListener(this);		

		p.add(detail);		
		detail.addActionListener(this);
		
		

		p.add(cancel);
		cancel.addActionListener(this);

		getContentPane().add(p);				
		head.setBounds(300,5,350,75);
		head.setFont(fnt);

		lno.setBounds(100,70,100,25);
		cno.setBounds(300,70,120,25);

		lname.setBounds(100,100,100,25);
		tname.setBounds(300,100,100,25);
		tname.setEnabled(false);

		slstop.setBounds(420,100,200,25);
		ststop.setBounds(600,100,100,25);
		ststop.setEnabled(false);

		//slmod.setBounds(420,130,200,25);
		//stmod.setBounds(600,130,100,25);
		//stmod.setEnabled(false);


		lsource.setBounds(100,130,100,25);
		tsource.setBounds(300,130,100,25);
		tsource.setEnabled(false);

		ldestination.setBounds(100,160,120,25);
		tdestination.setBounds(300,160,100,25);
		tdestination.setEnabled(false);
	
		sldist.setBounds(420,160,200,25);
		stdist.setBounds(600,160,100,25);
		stdist.setEnabled(false);

		ltype.setBounds(100,190,100,25);
		ctype.setBounds(300,190,100,25);
		ctype.setEnabled(false);

		larriv.setBounds(100,220,100,25);
		tarriv.setBounds(300,220,100,25);
		tarriv.setEnabled(false);
	
		slarriv.setBounds(420,220,120,25);
		starriv.setBounds(600,220,100,25);
		starriv.setEnabled(false);

		ldepart.setBounds(100,250,100,25);
		tdepart.setBounds(300,250,100,25);
		tdepart.setEnabled(false);

		sldepart.setBounds(420,250,120,25);
		stdepart.setBounds(600,250,100,25);
		stdepart.setEnabled(false);
		
		lday.setBounds(100,280,100,25);
		cday.setBounds(300,280,100,25);
		cday1.setBounds(400,280,150,25);
		cday.setEnabled(false);
		cday1.setEnabled(false);

		lcoach.setBounds(100,310,120,25);
		tcoach.setBounds(300,310,100,25);
		tcoach.setEnabled(false);
		
		searcha.setBounds(200,400,120,25);
		search.setBounds(100,400,100,25);
		stop.setBounds(300,400,100,25);
		detail.setBounds(400,400,100,25);		
		cancel.setBounds(500,400,100,25);

		setSize(800,600);
		setVisible(true);
		fill();
		//fillstoppage();		
	}
	public void actionPerformed(ActionEvent ae)
	{
		if(ae.getSource()==search)
		{
			try
			{
				Connection con;
				Statement st;
				ResultSet rs;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:railway");
				st=con.createStatement();						
                                String sql="select *from railway where trainno='"+cno.getSelectedItem()+"'";
				//Integer.parseInt(tno).getInt()";
                                rs=st.executeQuery(sql);
				if(rs.next())
				{				
					tname.setText(rs.getString(2));
					tsource.setText(rs.getString(3));
					tdestination.setText(rs.getString(4));
					ctype.setSelectedItem(rs.getString(5));
					tarriv.setText(rs.getString(6));
					tdepart.setText(rs.getString(7));
					cday.setSelectedItem(rs.getString(8));
					cday1.setSelectedItem(rs.getString(9));
					tcoach.setText(rs.getString(10));
					//stop.setEnabled(true);
				}			
				else
				{
					JOptionPane.showMessageDialog(new JFrame(),"Not Found");
					cno.setSelectedItem(this);
					tname.setText("");
					tsource.setText("");
					tdestination.setText("");
					ctype.setSelectedItem(this);
					tarriv.setText("");
					tdepart.setText("");
					cday.setSelectedItem(this);
					cday1.setSelectedItem(this);
					tcoach.setText("");
					stop.setEnabled(false);
				}
			}
			catch(Exception ex)
			{
				System.out.println("ERROR"+ex);
			}			
		}	
		if(ae.getSource()==stop)
		{
			try
			{
				Connection con;
				ResultSet rs;
				Statement st;
				String sql;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				con=DriverManager.getConnection("jdbc:odbc:stoppage");
				st=con.createStatement();
				sql="select *from stoppage where stoppage='"+ststop.getSelectedItem()+"'";
				st=con.createStatement();
				rs=st.executeQuery(sql);				
			}
			catch(Exception ex)
			{
				System.out.println("Error"+ex);	
			}
			ststop.removeAllItems();
			fillstoppage();		
			cno.setEnabled(false);
			tname.setEnabled(false);			
			ststop.setEnabled(true);			
			//stmod.setEnabled(true);
			tsource.setEnabled(false);
			tdestination.setEnabled(false);
			stdist.setEnabled(true);
			tarriv.setEnabled(false);
			starriv.setEnabled(true);
			tdepart.setEnabled(false);			
			stdepart.setEnabled(true);
			tcoach.setEnabled(false);
			ctype.setEnabled(false);
			cday.setEnabled(false);
			cday1.setEnabled(false);			
			
		}
		if(ae.getSource()==detail)
		{
			try
			{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver")	;
				Connection con=DriverManager.getConnection("jdbc:odbc:railway");
				Statement st3=con.createStatement();
				String sql3;
				System.out.println("correct");
				sql3="select *from stoppage where stoppage='"+ststop.getSelectedItem()+"'";
				
				ResultSet rs=st3.executeQuery(sql3);
				if(rs.next())
				{
					stdist.setText(rs.getString(3));
					starriv.setText(rs.getString(4));
					stdepart.setText(rs.getString(5));
				}
			}	
			catch(Exception ex)
			{
				System.out.println("Ex"+ex);
			}
		}
				
		if(ae.getSource()==cancel)
		{
			int reply=JOptionPane.showConfirmDialog(null,"Are U Sure ??");
			if(reply==JOptionPane.YES_OPTION)
			{				
				dispose();
			}
		}
		if(ae.getSource()==searcha)
		{		
			ststop.removeAllItems();
			cno.setEnabled(true);	
			tname.setEnabled(false);
			tsource.setEnabled(false);
			tdestination.setEnabled(false);
			ststop.setSelectedItem(this);
			tarriv.setEnabled(false);
			tdepart.setEnabled(false);
			tcoach.setEnabled(false);
			ctype.setEnabled(false);
			cday.setEnabled(false);
			cday1.setEnabled(false);
		}
			
	}
	public static void main(String args[])
	{
		new stoppage_search_no();
	}
	public void fill()
	{
		try	
		{
			Connection con;
			Statement st;
			ResultSet rs;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:railway");
			st=con.createStatement();				
			rs=st.executeQuery("select *from railway");
			while(rs.next())
			{
				cno.addItem(rs.getString(1));
			}
		}
		catch(Exception ex)
		{
		}
	}
	public void fillstoppage()
	{
		try
		{
			Connection con;
			Statement st;
			ResultSet rs;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con=DriverManager.getConnection("jdbc:odbc:stoppage");
			st=con.createStatement();				
			rs=st.executeQuery("select *from stoppage where tno='"+cno.getSelectedItem()+"'");
				while(rs.next())
				{
					ststop.addItem(rs.getString(2));
				}	
			
		}
		catch(Exception ex)
		{
			System.out.println("Stoppage Error"+ex);
		}				
	}
			
}

